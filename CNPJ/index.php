<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    class DataGenerator {
        public static function prepare($CNPJ) {
            $CNPJ = preg_replace('/[^0-9]/','',$CNPJ);
            if(strlen($CNPJ) != 14)
                return '';
            return preg_replace('/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/', '$1.$2.$3/$4-$5', $CNPJ);
        }
        public static function CNPJ() {
            $CNPJ = array("value" => array(), "query" => 0);
            for($i=0, $x = 5;$i<12;$i++, $x--) {
                if($x == 1)
                    $x = 9;
                $CNPJ['value'][$i] = intval(rand(0,9));
                $CNPJ['query'] += $CNPJ['value'][$i]*$x;
            }
            $query = intval($CNPJ['query']%11);
            $CNPJ['value'][12] = $query < 2 ? 0 : 11-$query;
            $CNPJ['query'] = 0;
            for($i=0, $x = 6; $i < 13; $i++, $x--) {
                if($x == 1)
                    $x = 9;
                $CNPJ['query'] += $CNPJ['value'][$i]*$x;
            }
            $query = intval($CNPJ['query']%11);
            $CNPJ['value'][13] = $query < 2 ? 0 : 11-$query;
            $value = "";
            for($i = 0; $i < 14; $i++)
                $value.=$CNPJ['value'][$i]."";
            return self::prepare($value);
        }
    }
    exit(DataGenerator::CNPJ());
?>
