<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    class DataGenerator {
        public static function prepare($CPF) {
            $CPF = preg_replace('/[^0-9]/', '', $CPF);
            if(strlen($CPF) != 11) {
                return '';
            }
            return preg_replace('/^(\d{3})(\d{3})(\d{3})(\d{2})$/', '$1.$2.$3-$4', $CPF);
        }
        public static function CPF() {
            $CPF = array("value" => array(), "query" => 0);
            for($i=0, $x = 10;$i<9;$i++, $x--) {
                $CPF['value'][$i] = intval(rand(0,9));
                $CPF['query'] += $CPF['value'][$i]*$x;
            }
            $query = intval($CPF['query']%11);
            $CPF['value'][9] = $query < 2 ? 0 : 11-$query;
            $CPF['query'] = 0;
            for($i=0, $x = 11; $i < 10; $i++, $x--)
                $CPF['query'] += $CPF['value'][$i]*$x;
            $query = intval($CPF['query']%11);
            $CPF['value'][10] = $query < 2 ? 0 : 11-$query;
            $value = "";
            for($i = 0; $i < 11; $i++)
                $value.=$CPF['value'][$i]."";
            return self::prepare($value);
        }
    }
    exit(DataGenerator::CPF());
?>
